package br.com.ballarati._2022_gitlab_api_musicas.service;

import br.com.ballarati._2022_gitlab_api_musicas.model.Musica;
import br.com.ballarati._2022_gitlab_api_musicas.repository.MusicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.swing.plaf.metal.MetalSeparatorUI;
import java.util.List;


@Service
public class MusicaService {

    @Autowired
    private MusicaRepository musicaRepository;

    public void cadastrar(Musica musica){
        musicaRepository.save(musica);
        musica.setAvaliacao(0);

    }
    public List <Musica> listar(){
        return musicaRepository.findAll((Sort.by(Sort.Direction.DESC, "avaliacao")));
    }

    public void avaliar(Musica musica){
        musica.setAvaliacao(musica.getAvaliacao());
    }
}
