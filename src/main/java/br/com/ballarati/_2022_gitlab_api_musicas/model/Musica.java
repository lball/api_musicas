package br.com.ballarati._2022_gitlab_api_musicas.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Musica {
    @Id
    @GeneratedValue
    private Long id;

    private String nome;

    private Integer avaliacao;
}
