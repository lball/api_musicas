package br.com.ballarati._2022_gitlab_api_musicas.controller;

import br.com.ballarati._2022_gitlab_api_musicas.model.Musica;
import br.com.ballarati._2022_gitlab_api_musicas.service.MusicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/musicas")
public class MusicaController {

    @Autowired
    private MusicaService musicaService;

    @GetMapping
    public List<Musica> listar () {
        return this.musicaService.listar();
    }

    @PostMapping("/cadastrar")
    public void cadastrar (@RequestBody Musica musica){
        musicaService.cadastrar(musica);
        }

    @PatchMapping("/avaliar")
    public void avaliar (@RequestBody Musica musica){
        musicaService.avaliar(musica);
        }

    }

